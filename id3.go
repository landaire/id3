package id3

import "errors"

// Version represents an ID3 frame version
type Version int

type Speed byte

const (
	// Version1 is ID3v1
	Version1 Version = iota
	// Version2 is ID3v2.{3,4}
	Version2
)

const (
	// TagMagic represents the magic number in the header of an ID3v1 frame
	TagMagic = "TAG"
	// TagExtendedMagic is the magic for extended tag information
	TagExtendedMagic = "TAG+"
)

const (
	SpeedUnset Speed = iota
	SpeedSlow
	SpeedMedium
	SpeedFast
	SpeedHardcore
)

var (
	// ErrTrackNotSupported is used when a frame does not support track numbers
	// (only in ID3v1)
	ErrTrackNotSupported = errors.New("Track is not supported by this ID3 frame")
)

// Frame represents an ID3 frame of type 1 or 2
type Frame interface {
	// Version returns the ID3 frame version
	Version() int
	// HasTrack returns whether or not the given frame supports tracks
	HasTrack() bool
	// Track returns the track number or error if track is not supported
	Track() int
}

type ExtendedTag struct {
	magic     [4]byte
	title     [60]byte
	artist    [60]byte
	album     [60]byte
	speed     Speed
	genre     [30]byte
	startTime [6]byte
	endTime   [6]byte
}

// Framev1 represents an ID3v1 frame
type Framev1 struct {
	magic   [4]byte
	title   [30]byte
	artist  [30]byte
	album   [30]byte
	year    [4]byte
	comment [30]byte
}

// Version of this ID3 frame
func (f Framev1) Version() Version {
	return Version1
}

// HasTrack returns whether or not the given frame supports tracks
func (f Framev1) HasTrack() bool {
	// If the byte at index 28 is 0, byte 29 is the track
	return f.comment[len(f.comment)-2] == 0
}

// Track returns the track number or error if track is not supported
func (f Framev1) Track() int {
	return int(f.comment[len(f.comment)-1])
}
